/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

// *Note:
// Для подключения отладчика по SWD необходимо установить настройки альтернативной функции выводов
// Выбрать в настройках отладочной конфигурации Connect under reset
// При первом программировании соединить NRST отладчика с пином Reset процессора (R)




/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

//Saturate X at +/-VAL_MAX
#define SAT(x,VAL_MAX,VAL_MIN) ( ((x) > VAL_MAX) ? VAL_MAX : ( ( (x) < VAL_MIN) ? (VAL_MIN) : (x) ) )
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */
uint32_t adc_buf[2];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

volatile uint32_t uwTim1ReloadCtr = 500;
volatile uint32_t uwTim1Reload500Ctr = 4;
volatile int32_t wAdcDR = 0;
volatile uint32_t uwTim2Pulse = 500;
volatile uint32_t uwAvrgCount0 = 3; // current
volatile uint32_t uwAvrgCount1 = 4;  //  pos
volatile uint32_t uwAvrgCount2 = 3;  //  current hard filter
volatile uint32_t uwAvCur = 1;

#define cEMAGTOHALLMUL (248/100) //coefficients of linear combination of influence
#define cEMAGTOHALLADD (2032)    // of electromagnet field produced by coil supplied with PWM on  HALL sensor measured value.

volatile int32_t wpPWM = 0; // set to PWM output channel
volatile int32_t wpPWMS = 0; // set to saturated PWM output channel


volatile int32_t wpCUR_SET = 300; // set to PWM output channel

volatile int32_t wpCUR = 0; // target current

volatile int32_t wpTRGPROXSET = 0; // target proximity
volatile int32_t wpHALLSENSFLT = 0; // Hall sensor filtered
volatile int32_t wpCUR_ERR = 0;
volatile int32_t wpCUR_ERR_DIF = 0;
volatile int32_t wpCUR_ERR_INT = 0;
volatile int32_t wpCUR_ERR_PRV = 0;

volatile int32_t wqCUR_P = 150;
volatile int32_t wqCUR_D = 0;
volatile int32_t wqCUR_I = 150;

volatile uint32_t uwpPWMENABLE = 1;

volatile uint32_t uwpPWMENABLE1 = 0;
volatile uint32_t wpPWMPULSE_ctr = 0;

volatile int32_t wAdcDR0, wAdcDR1,wAdcDR0f,wAdcDR1f,wAdcDR0f2;



volatile int32_t wpPOS_ERR = 0;
volatile int32_t wpPOS = 0;
volatile int32_t wpPOSc = 0;
volatile int32_t wpPOS_SET = 1300;//3250; // position setpoint

volatile int32_t wpPOS_ERR_DIF;
volatile int32_t wpPOS_ERR_PRV;

volatile int32_t wpPOS_ERR_INT;


volatile int32_t wpCUR_SET0;

volatile int32_t wqPOS_P=6000;
volatile int32_t wqPOS_D=25000;
volatile int32_t wqPOS_I=0;

volatile int32_t wpCUR_SET_ctr=0;
volatile int32_t wqPwr = 0;




int ctr0 = 0;
int ctr1 = 0;

int adc_seq = 2;





void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	 UNUSED(htim);

	 adc_seq = 2;
//	 HAL_ADC_Start_IT(&hadc1);
	 HAL_ADC_Start_DMA(&hadc1, adc_buf, 2);




}
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	 HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);


			wAdcDR0 = adc_buf[0];
			wAdcDR1 = adc_buf[1];




	if (1){

		//wpPOS_SET = (posz,1100,950);



		wAdcDR0f = ((uwAvrgCount0-1)*wAdcDR0f + wAdcDR0)/uwAvrgCount0; // current adc
		wAdcDR0f2 = ((uwAvrgCount2-1)*wAdcDR0f2 + wAdcDR0)/uwAvrgCount2; // current adc, hard filtered
		wAdcDR1f = ((uwAvrgCount1-1)*wAdcDR1f + wAdcDR1)/uwAvrgCount1;

		// read state variables from sensors
		wpPOS 			= wAdcDR1f;
		wpCUR 			= wAdcDR0f;


		//0.246*cadc+2124; //small coil
		//wpPOSc = wpPOS-(wpCUR*553+2107933)/1000; // initial coil


		wpPOSc = wpPOS-(wAdcDR0f2*246+2124000)/1000; // initial coil



		if (wpPOSc<1000) {
			uwpPWMENABLE1 = 0;
		}else
		uwpPWMENABLE1 = 1;


		// main control loop
		wpPOS_ERR 		= wpPOSc-wpPOS_SET; // position regulation error
		wpPOS_ERR_DIF 	= wpPOS_ERR-wpPOS_ERR_PRV; //derivative of error
		wpPOS_ERR_PRV 	= wpPOS_ERR;
		wpPOS_ERR_INT 	= SAT(wpPOS_ERR_INT+wpPOS_ERR,10000,-10000); // integral part of error


		wpCUR_SET0 			= -(wpPOS_ERR*wqPOS_P+wpPOS_ERR_DIF*wqPOS_D+wpPOS_ERR_INT*wqPOS_I)/1000;

		if(wpCUR_SET_ctr==0)
			wpCUR_SET = ((uwAvCur-1)*wpCUR_SET + SAT(wpCUR_SET0,4000,0))/uwAvCur;

		// pid for current

		wpCUR_ERR 		= wpCUR-wpCUR_SET; // current regulation error
		wpCUR_ERR_DIF 	= wpCUR_ERR-wpCUR_ERR_PRV; //derivative of error
		wpCUR_ERR_PRV 	= wpCUR_ERR;
		wpCUR_ERR_INT 	= SAT(wpCUR_ERR_INT+wpCUR_ERR,10000,-10000); // integral part of error

		wpPWM 			= -(wpCUR_ERR*wqCUR_P+wpCUR_ERR_DIF*wqCUR_D+wpCUR_ERR_INT*wqCUR_I + wqPwr)/1000;

		if(wpPWMPULSE_ctr==0){
			wpPWMS = SAT(wpPWM,999/2,0);
		}else{
			uwpPWMENABLE1 = 1;
		}





		// TODO add detection of open loop (power supply, no reaction on input set)


		if (uwpPWMENABLE&& uwpPWMENABLE1){
			htim2.Instance->CCR1 =wpPWMS;
		}else{
			htim2.Instance->CCR1 =0;
		}




		if (--uwTim1ReloadCtr==0){
			uwTim1ReloadCtr = 500;
			switch(--uwTim1Reload500Ctr){
			case 3:
				HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);
				break;
			case 2:
				HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
				break;
			case 1:
				HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_13);
				break;
			case 0:
				uwTim1Reload500Ctr = 4;
				HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
				break;
			default:

				break;
			}
		}



	}
	return;


}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
 HAL_TIM_Base_Start_IT(&htim1);
  HAL_TIM_Base_Start_IT(&htim2);

  TIM_CCxChannelCmd(htim2.Instance, TIM_CHANNEL_1, TIM_CCx_ENABLE);
  TIM_CCxChannelCmd(htim1.Instance, TIM_CHANNEL_1, TIM_CCx_ENABLE) ;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 72;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 500;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
